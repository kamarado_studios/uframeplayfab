using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
public class PlayFabErrorConsoleView : PlayFabErrorConsoleViewBase
{
	public Text MessagesText;
	public int LineLimit;
	protected override void InitializeViewModel(ViewModel model)
	{
		base.InitializeViewModel(model);
	}
	public override void Bind()
	{
		base.Bind();
		MessagesText.text = "";
	}
	public override void MessagesAdded(string arg1)
	{
		base.MessagesAdded(arg1);
		var str = PlayFabErrorConsole.Messages.TakeLast(20).Aggregate("", (prev, cur) => prev + (cur + "\n"));
		MessagesText.text = str;
	}
}
public static class Extensions
{
	public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> collection, int n)
	{
		if (collection == null)
			throw new ArgumentNullException("collection");
		if (n < 0)
			throw new ArgumentOutOfRangeException("n", "n must be 0 or greater");
		LinkedList<T> temp = new LinkedList<T>();
		foreach (var value in collection) {
			temp.AddLast(value);
			if (temp.Count > n)
				temp.RemoveFirst();
		}
		return temp;
	}
}
