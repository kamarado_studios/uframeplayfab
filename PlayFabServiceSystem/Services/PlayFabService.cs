using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml.Linq;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Internal;
using PlayFabServiceSystem;
using UniRx;

public class PlayFabService : PlayFabServiceBase
{
    public override void Setup()
    {
        base.Setup();
        // This is called when the controller is created

        LoginStateProperty = new P<PlayFabLoginState>(PlayFabLoginState.LoggedOut);
        LoginTypeProperty = new P<PlayFabAuthType>(PlayFabAuthType.None);
        LocalAccountInfoProperty = new P<UserAccountInfo>(null);
        PlayFabSettings.TitleId = TitleId;

    }

    public string TitleId;

    public string PublisherId;

    public P<PlayFabAuthType> LoginTypeProperty { get; private set; }

    public P<PlayFabLoginState> LoginStateProperty { get; private set; }
    public P<UserAccountInfo> LocalAccountInfoProperty { get; private set; }


    public PlayFabLoginState LoginState
    {
        get { return LoginStateProperty.Value; }
        set { LoginStateProperty.Value = value; }
    }

    public PlayFabAuthType LoginType
    {
        get { return LoginTypeProperty.Value; }
        set { LoginTypeProperty.Value = value; }
    }

    public UserAccountInfo LocalAccountInfo
    {
        get { return LocalAccountInfoProperty.Value; }
        set { LocalAccountInfoProperty.Value = value; }
    }

    public IObservable<RegisterPlayFabUserResult> RegisterPlayFabUser(string username, string password, string email,
        string origination = null)
    {
        var request = new RegisterPlayFabUserRequest
        {
            Username = username,
            Password = password,
            PublisherId = PublisherId,
            TitleId = TitleId,
            Email = email,
            Origination = origination
        };
        return RegisterPlayFabUser(request);
    }

    public IObservable<RegisterPlayFabUserResult> RegisterPlayFabUser(RegisterPlayFabUserRequest request)
    {
        var observable = new Subject<RegisterPlayFabUserResult>();

        this.Publish(new RegisterPlayFabUserEvent
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RegisterPlayFabUser(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RegisterPlayFabUserEvent
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RegisterPlayFabUserEvent
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LoginResult> LoginWithPlayFab(string username, string password)
    {
        var request = new LoginWithPlayFabRequest
        {
            Username = username,
            Password = password,
            PublisherId = PublisherId,
            TitleId = TitleId
        };

        return LoginWithPlayFab(request);
    }

    public IObservable<LoginResult> LoginWithPlayFab(LoginWithPlayFabRequest request)
    {
        if (LoginState == PlayFabLoginState.LoggedIn) throw new Exception("Cannot login while logged in!");
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.PlayFab,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithPlayFab(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.PlayFab;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.PlayFab,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });


                this.GetAccountInfo(new GetAccountInfoRequest()).Subscribe(_ =>
                {
                    this.LocalAccountInfo = _.AccountInfo;
                });

            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.PlayFab,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }
    public IObservable<LoginResult> LoginWithAndroidDeviceID(string androidDeviceId, bool? createAccount = null, string androidDevice = null, string os = null)
    {
        var request = new LoginWithAndroidDeviceIDRequest
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            AndroidDevice = androidDevice,
            AndroidDeviceId = androidDeviceId,
            CreateAccount = createAccount,
            OS = os
        };

        return LoginWithAndroidDeviceID(request);
    }

    public IObservable<LoginResult> LoginWithAndroidDeviceID(LoginWithAndroidDeviceIDRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.Android,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithAndroidDeviceID(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.PlayFab;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Android,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Android,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }

    public IObservable<LoginResult> LoginWithEmailAddress(string email, string password)
    {
        var request = new LoginWithEmailAddressRequest
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            Email = email,
            Password = password
        };

        return LoginWithEmailAddress(request);
    }

    public IObservable<LoginResult> LoginWithEmailAddress(LoginWithEmailAddressRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.Email,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithEmailAddress(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.Email;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Email,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Email,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }


    public IObservable<LoginResult> LoginWithFacebook(string accessToken, bool? createAccount)
    {
        var request = new LoginWithFacebookRequest()
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            AccessToken = accessToken,
            CreateAccount = createAccount

        };

        return LoginWithFacebook(request);
    }

    public IObservable<LoginResult> LoginWithFacebook(LoginWithFacebookRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.Facebook,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithFacebook(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.Facebook;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Facebook,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Facebook,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }

    public IObservable<LoginResult> LoginWithGameCenter(string playerId, bool? createAccount)
    {
        var request = new LoginWithGameCenterRequest()
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            PlayerId = playerId,
            CreateAccount = createAccount

        };

        return LoginWithGameCenter(request);
    }

    public IObservable<LoginResult> LoginWithGameCenter(LoginWithGameCenterRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.GameCenter,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithGameCenter(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.GameCenter;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.GameCenter,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.GameCenter,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }

    public IObservable<LoginResult> LoginWithGoogleAccount(string accessToken, bool? createAccount)
    {
        var request = new LoginWithGoogleAccountRequest()
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            AccessToken = accessToken,
            CreateAccount = createAccount

        };

        return LoginWithGoogleAccount(request);
    }

    public IObservable<LoginResult> LoginWithGoogleAccount(LoginWithGoogleAccountRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.GoogleAccount,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithGoogleAccount(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.GoogleAccount;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.GoogleAccount,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.GoogleAccount,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }

    public IObservable<LoginResult> LoginWithIOSDeviceID(string deviceId, bool? createAccount, string os = null, string deviceModel = null)
    {
        var request = new LoginWithIOSDeviceIDRequest()
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            DeviceId = deviceId,
            CreateAccount = createAccount,
            OS = os,
            DeviceModel = deviceModel

        };

        return LoginWithIOSDeviceID(request);
    }

    public IObservable<LoginResult> LoginWithIOSDeviceID(LoginWithIOSDeviceIDRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.IOS,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithIOSDeviceID(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.IOS;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.IOS,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.IOS,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }

    public IObservable<LoginResult> LoginWithSteam(string steamTicket, bool? createAccount)
    {
        var request = new LoginWithSteamRequest()
        {
            PublisherId = PublisherId,
            TitleId = TitleId,
            SteamTicket = steamTicket,
            CreateAccount = createAccount,
        };

        return LoginWithSteam(request);
    }

    public IObservable<LoginResult> LoginWithSteam(LoginWithSteamRequest request)
    {
        var observable = new Subject<LoginResult>();

        this.Publish(new LoginEvent
        {
            AuthType = PlayFabAuthType.Steam,
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        LoginState = PlayFabLoginState.LoggingIn;

        PlayFabClientAPI.LoginWithSteam(request,
            result =>
            {
                observable.OnNext(result);
                LoginState = PlayFabLoginState.LoggedIn;
                LoginType = PlayFabAuthType.Steam;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Steam,
                    Result = result,
                    OperationState = PlayFabOperationState.Complete,
                    Request = request
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                LoginState = PlayFabLoginState.LoggedOut;
                this.Publish(new LoginEvent
                {
                    AuthType = PlayFabAuthType.Steam,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed,
                    Request = request
                });
            });

        return observable.Take(1);
    }

    public IObservable<AddUsernamePasswordResult> AddUsernamePassword(string username, string password, string email)
    {
        var request = new AddUsernamePasswordRequest
        {
            Username = username,
            Password = password,
            PublisherId = PublisherId,
            Email = email,
        };
        return AddUsernamePassword(request);
    }

    public IObservable<AddUsernamePasswordResult> AddUsernamePassword(AddUsernamePasswordRequest request)
    {
        var observable = new Subject<AddUsernamePasswordResult>();

        this.Publish(new AddUsernamePasswordEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.AddUsernamePassword(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new AddUsernamePasswordEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new AddUsernamePasswordEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetAccountInfoResult> GetAccountInfo(string playFabId)
    {
        var request = new GetAccountInfoRequest
        {
            PlayFabId = playFabId,
        };
        return GetAccountInfo(request);
    }

    public IObservable<GetAccountInfoResult> GetAccountInfo(GetAccountInfoRequest request)
    {
        var observable = new Subject<GetAccountInfoResult>();

        this.Publish(new GetAccountInfoEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetAccountInfo(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetAccountInfoEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetAccountInfoEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<GetPlayFabIDsFromFacebookIDsResult> GetPlayFabIDsFromFacebookIDs(List<string> facebookIDs)
    {
        var request = new GetPlayFabIDsFromFacebookIDsRequest
        {
            FacebookIDs = facebookIDs,
            PublisherId = PublisherId
        };
        return GetPlayFabIDsFromFacebookIDs(request);
    }

    public IObservable<GetPlayFabIDsFromFacebookIDsResult> GetPlayFabIDsFromFacebookIDs(GetPlayFabIDsFromFacebookIDsRequest request)
    {
        var observable = new Subject<GetPlayFabIDsFromFacebookIDsResult>();

        this.Publish(new GetPlayFabIDsFromFacebookIDsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetPlayFabIDsFromFacebookIDs(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetPlayFabIDsFromFacebookIDsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetPlayFabIDsFromFacebookIDsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetUserCombinedInfoResult> GetUserCombinedInfo(string playFabId = null, bool? getAccountInfo = null, bool? getInventory = null,
        bool? getVirtualCurrency = null, bool? getUserData = null, bool? getReadOnlyData = null, List<string> userDataKeys = null, List<string> readOnlyDataKeys = null)
    {
        var request = new GetUserCombinedInfoRequest
        {
            GetAccountInfo = getAccountInfo,
            GetInventory = getInventory,
            GetReadOnlyData = getReadOnlyData,
            GetUserData = getUserData,
            GetVirtualCurrency = getVirtualCurrency,
            PlayFabId = playFabId,
            ReadOnlyDataKeys = readOnlyDataKeys,
            UserDataKeys = userDataKeys

        };
        return GetUserCombinedInfo(request);
    }

    public IObservable<GetUserCombinedInfoResult> GetUserCombinedInfo(GetUserCombinedInfoRequest request)
    {
        var observable = new Subject<GetUserCombinedInfoResult>();

        this.Publish(new GetUserCombinedInfoEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserCombinedInfo(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserCombinedInfoEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserCombinedInfoEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LinkAndroidDeviceIDResult> LinkAndroidDeviceID(string androidDeviceId, string androidDevice = null, string os = null)
    {
        var request = new LinkAndroidDeviceIDRequest()
        {
            AndroidDevice = androidDevice,
            AndroidDeviceId = androidDeviceId,
            OS = os,
            PublisherId = PublisherId
        };
        return LinkAndroidDeviceID(request);
    }

    public IObservable<LinkAndroidDeviceIDResult> LinkAndroidDeviceID(LinkAndroidDeviceIDRequest request)
    {
        var observable = new Subject<LinkAndroidDeviceIDResult>();

        this.Publish(new LinkAndroidDeviceIDEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.LinkAndroidDeviceID(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new LinkAndroidDeviceIDEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new LinkAndroidDeviceIDEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LinkFacebookAccountResult> LinkFacebookAccount(string accessToken)
    {
        var request = new LinkFacebookAccountRequest()
        {
            AccessToken = accessToken,
            PublisherId = PublisherId
        };
        return LinkFacebookAccount(request);
    }

    public IObservable<LinkFacebookAccountResult> LinkFacebookAccount(LinkFacebookAccountRequest request)
    {
        var observable = new Subject<LinkFacebookAccountResult>();

        this.Publish(new LinkFacebookAccountEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.LinkFacebookAccount(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new LinkFacebookAccountEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new LinkFacebookAccountEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LinkGameCenterAccountResult> LinkGameCenterAccount(string gameCenterId)
    {
        var request = new LinkGameCenterAccountRequest()
        {
            GameCenterId = gameCenterId,
            PublisherId = PublisherId
        };
        return LinkGameCenterAccount(request);
    }

    public IObservable<LinkGameCenterAccountResult> LinkGameCenterAccount(LinkGameCenterAccountRequest request)
    {
        var observable = new Subject<LinkGameCenterAccountResult>();

        this.Publish(new LinkGameCenterAccountEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.LinkGameCenterAccount(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new LinkGameCenterAccountEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new LinkGameCenterAccountEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LinkIOSDeviceIDResult> LinkIOSDeviceID(string deviceId, string deviceModel = null, string os = null)
    {
        var request = new LinkIOSDeviceIDRequest()
        {
            DeviceId = deviceId,
            DeviceModel = deviceModel,
            OS = os,
            PublisherId = PublisherId
        };
        return LinkIOSDeviceID(request);
    }

    public IObservable<LinkIOSDeviceIDResult> LinkIOSDeviceID(LinkIOSDeviceIDRequest request)
    {
        var observable = new Subject<LinkIOSDeviceIDResult>();

        this.Publish(new LinkIOSDeviceIDEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.LinkIOSDeviceID(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new LinkIOSDeviceIDEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new LinkIOSDeviceIDEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LinkSteamAccountResult> LinkSteamAccount(string steamTicket)
    {
        var request = new LinkSteamAccountRequest()
        {
            SteamTicket = steamTicket,
            PublisherId = PublisherId
        };
        return LinkSteamAccount(request);
    }

    public IObservable<LinkSteamAccountResult> LinkSteamAccount(LinkSteamAccountRequest request)
    {
        var observable = new Subject<LinkSteamAccountResult>();

        this.Publish(new LinkSteamAccountEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.LinkSteamAccount(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new LinkSteamAccountEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new LinkSteamAccountEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<SendAccountRecoveryEmailResult> LinkSteamAcSendAccountRecoveryEmailcount(string email)
    {
        var request = new SendAccountRecoveryEmailRequest()
        {
            Email = email,
            TitleId = TitleId,
            PublisherId = PublisherId
        };
        return SendAccountRecoveryEmail(request);
    }

    public IObservable<SendAccountRecoveryEmailResult> SendAccountRecoveryEmail(SendAccountRecoveryEmailRequest request)
    {
        var observable = new Subject<SendAccountRecoveryEmailResult>();

        this.Publish(new SendAccountRecoveryEmailEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.SendAccountRecoveryEmail(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new SendAccountRecoveryEmailEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new SendAccountRecoveryEmailEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<UnlinkAndroidDeviceIDResult> UnlinkAndroidDeviceID()
    {
        var request = new UnlinkAndroidDeviceIDRequest();
        return UnlinkAndroidDeviceID(request);
    }

    public IObservable<UnlinkAndroidDeviceIDResult> UnlinkAndroidDeviceID(UnlinkAndroidDeviceIDRequest request)
    {
        var observable = new Subject<UnlinkAndroidDeviceIDResult>();

        this.Publish(new UnlinkAndroidDeviceIDEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UnlinkAndroidDeviceID(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UnlinkAndroidDeviceIDEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UnlinkAndroidDeviceIDEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<UnlinkFacebookAccountResult> UnlinkFacebookAccount()
    {
        var request = new UnlinkFacebookAccountRequest();
        return UnlinkFacebookAccount(request);
    }

    public IObservable<UnlinkFacebookAccountResult> UnlinkFacebookAccount(UnlinkFacebookAccountRequest request)
    {
        var observable = new Subject<UnlinkFacebookAccountResult>();

        this.Publish(new UnlinkFacebookAccountEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UnlinkFacebookAccount(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UnlinkFacebookAccountEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UnlinkFacebookAccountEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<UnlinkGameCenterAccountResult> UnlinkGameCenterAccount()
    {
        var request = new UnlinkGameCenterAccountRequest();
        return UnlinkGameCenterAccount(request);
    }

    public IObservable<UnlinkGameCenterAccountResult> UnlinkGameCenterAccount(UnlinkGameCenterAccountRequest request)
    {
        var observable = new Subject<UnlinkGameCenterAccountResult>();

        this.Publish(new UnlinkGameCenterAccountEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UnlinkGameCenterAccount(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UnlinkGameCenterAccountEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UnlinkGameCenterAccountEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }



    public IObservable<UnlinkIOSDeviceIDResult> UnlinkIOSDeviceID()
    {
        var request = new UnlinkIOSDeviceIDRequest();
        return UnlinkIOSDeviceID(request);
    }

    public IObservable<UnlinkIOSDeviceIDResult> UnlinkIOSDeviceID(UnlinkIOSDeviceIDRequest request)
    {
        var observable = new Subject<UnlinkIOSDeviceIDResult>();

        this.Publish(new UnlinkIOSDeviceIDEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UnlinkIOSDeviceID(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UnlinkIOSDeviceIDEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UnlinkIOSDeviceIDEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<UnlinkSteamAccountResult> UnlinkSteamAccount()
    {
        var request = new UnlinkSteamAccountRequest();
        return UnlinkSteamAccount(request);
    }

    public IObservable<UnlinkSteamAccountResult> UnlinkSteamAccount(UnlinkSteamAccountRequest request)
    {
        var observable = new Subject<UnlinkSteamAccountResult>();

        this.Publish(new UnlinkSteamAccountEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UnlinkSteamAccount(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UnlinkSteamAccountEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UnlinkSteamAccountEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<UpdateUserTitleDisplayNameResult> UpdateUserTitleDisplayName(string name)
    {
        var request = new UpdateUserTitleDisplayNameRequest()
        {
            DisplayName = name
        };
        return UpdateUserTitleDisplayName(request);
    }

    public IObservable<UpdateUserTitleDisplayNameResult> UpdateUserTitleDisplayName(UpdateUserTitleDisplayNameRequest request)
    {
        var observable = new Subject<UpdateUserTitleDisplayNameResult>();

        this.Publish(new UpdateUserTitleDisplayNameEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UpdateUserTitleDisplayName(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UpdateUserTitleDisplayNameEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UpdateUserTitleDisplayNameEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<GetLeaderboardResult> GetFriendLeaderboard(string statisticsName, int maxResCount, int startPosition)
    {
        var request = new GetFriendLeaderboardRequest()
        {
            MaxResultsCount = maxResCount,
            StartPosition = startPosition,
            StatisticName = statisticsName
        };
        return GetFriendLeaderboard(request);
    }

    public IObservable<GetLeaderboardResult> GetFriendLeaderboard(GetFriendLeaderboardRequest request)
    {
        var observable = new Subject<GetLeaderboardResult>();

        this.Publish(new GetFriendLeaderboardEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetFriendLeaderboard(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetFriendLeaderboardEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetFriendLeaderboardEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetLeaderboardResult> GetLeaderboard(string statisticsName, int maxResCount, int startPosition)
    {
        var request = new GetLeaderboardRequest()
        {
            MaxResultsCount = maxResCount,
            StartPosition = startPosition,
            StatisticName = statisticsName
        };
        return GetLeaderboard(request);
    }

    public IObservable<GetLeaderboardResult> GetLeaderboard(GetLeaderboardRequest request)
    {
        var observable = new Subject<GetLeaderboardResult>();

        this.Publish(new GetLeaderboardEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetLeaderboard(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetLeaderboardEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetLeaderboardEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<GetLeaderboardAroundCurrentUserResult> GetLeaderboardAroundCurrentUser(string statisticsName, int maxResCount)
    {
        var request = new GetLeaderboardAroundCurrentUserRequest()
        {
            MaxResultsCount = maxResCount,
            StatisticName = statisticsName
        };
        return GetLeaderboardAroundCurrentUser(request);
    }

    public IObservable<GetLeaderboardAroundCurrentUserResult> GetLeaderboardAroundCurrentUser(GetLeaderboardAroundCurrentUserRequest request)
    {
        var observable = new Subject<GetLeaderboardAroundCurrentUserResult>();

        this.Publish(new GetLeaderboardAroundCurrentUserEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetLeaderboardAroundCurrentUser(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetLeaderboardAroundCurrentUserEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetLeaderboardAroundCurrentUserEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetUserDataResult> GetUserData(List<string> keys, string playFabId)
    {
        var request = new GetUserDataRequest()
        {
            Keys = keys,
            PlayFabId = playFabId
        };
        return GetUserData(request);
    }

    public IObservable<GetUserDataResult> GetUserData(GetUserDataRequest request)
    {
        var observable = new Subject<GetUserDataResult>();

        this.Publish(new GetUserDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<GetUserDataResult> GetUserPublisherData(List<string> keys, string playFabId)
    {
        var request = new GetUserDataRequest()
        {
            Keys = keys,
            PlayFabId = playFabId
        };
        return GetUserPublisherData(request);
    }

    public IObservable<GetUserDataResult> GetUserPublisherData(GetUserDataRequest request)
    {
        var observable = new Subject<GetUserDataResult>();

        this.Publish(new GetUserPublisherDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserPublisherData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserPublisherDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserPublisherDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetUserDataResult> GetUserPublisherReadOnlyData(List<string> keys, string playFabId)
    {
        var request = new GetUserDataRequest()
        {
            Keys = keys,
            PlayFabId = playFabId
        };
        return GetUserPublisherReadOnlyData(request);
    }

    public IObservable<GetUserDataResult> GetUserPublisherReadOnlyData(GetUserDataRequest request)
    {
        var observable = new Subject<GetUserDataResult>();

        this.Publish(new GetUserPublisherReadOnlyDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserPublisherReadOnlyData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserPublisherReadOnlyDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserPublisherReadOnlyDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<GetUserDataResult> GetUserReadOnlyData(List<string> keys, string playFabId)
    {
        var request = new GetUserDataRequest()
        {
            Keys = keys,
            PlayFabId = playFabId
        };
        return GetUserReadOnlyData(request);
    }

    public IObservable<GetUserDataResult> GetUserReadOnlyData(GetUserDataRequest request)
    {
        var observable = new Subject<GetUserDataResult>();

        this.Publish(new GetUserPublisherReadOnlyDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserReadOnlyData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserReadOnlyDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserReadOnlyDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<GetUserStatisticsResult> GetUserStatistics()
    {
        var request = new GetUserStatisticsRequest();
        return GetUserStatistics(request);
    }

    public IObservable<GetUserStatisticsResult> GetUserStatistics(GetUserStatisticsRequest request)
    {
        var observable = new Subject<GetUserStatisticsResult>();

        this.Publish(new GetUserStatisticsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserStatistics(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserStatisticsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserStatisticsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<UpdateUserDataResult> UpdateUserData(Dictionary<string, string> data, UserDataPermission userDataPermission)
    {
        var request = new UpdateUserDataRequest()
        {
            Data = data,
            Permission = userDataPermission
        };
        return UpdateUserData(request);
    }

    public IObservable<UpdateUserDataResult> UpdateUserData(UpdateUserDataRequest request)
    {
        var observable = new Subject<UpdateUserDataResult>();

        this.Publish(new UpdateUserDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UpdateUserData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UpdateUserDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UpdateUserDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<UpdateUserDataResult> UpdateUserPublisherData(Dictionary<string, string> data, UserDataPermission userDataPermission)
    {
        var request = new UpdateUserDataRequest()
        {
            Data = data,
            Permission = userDataPermission
        };
        return UpdateUserData(request);
    }

    public IObservable<UpdateUserDataResult> UpdateUserPublisherData(UpdateUserDataRequest request)
    {
        var observable = new Subject<UpdateUserDataResult>();

        this.Publish(new UpdateUserPublisherDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UpdateUserPublisherData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UpdateUserPublisherDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UpdateUserPublisherDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<UpdateUserStatisticsResult> UpdateUserStatistics(Dictionary<string, int> userStatistics)
    {
        var request = new UpdateUserStatisticsRequest()
        {
            UserStatistics = userStatistics
        };
        return UpdateUserStatistics(request);
    }

    public IObservable<UpdateUserStatisticsResult> UpdateUserStatistics(UpdateUserStatisticsRequest request)
    {
        var observable = new Subject<UpdateUserStatisticsResult>();

        this.Publish(new UpdateUserStatisticsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UpdateUserStatistics(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UpdateUserStatisticsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UpdateUserStatisticsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<GetCatalogItemsResult> GetCatalogItems(string catalogVersion)
    {
        var request = new GetCatalogItemsRequest()
        {
            CatalogVersion = catalogVersion
        };
        return GetCatalogItems(request);
    }

    public IObservable<GetCatalogItemsResult> GetCatalogItems(GetCatalogItemsRequest request)
    {
        var observable = new Subject<GetCatalogItemsResult>();

        this.Publish(new GetCatalogItemsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetCatalogItems(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetCatalogItemsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetCatalogItemsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<GetStoreItemsResult> GetStoreItems(string storeId)
    {
        var request = new GetStoreItemsRequest()
        {
            StoreId = storeId
        };
        return GetStoreItems(request);
    }

    public IObservable<GetStoreItemsResult> GetStoreItems(GetStoreItemsRequest request)
    {
        var observable = new Subject<GetStoreItemsResult>();

        this.Publish(new GetStoreItemsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetStoreItems(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetStoreItemsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetStoreItemsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }



    public IObservable<GetTitleDataResult> GetTitleData(List<string> keys)
    {
        var request = new GetTitleDataRequest()
        {
            Keys = keys
        };
        return GetTitleData(request);
    }

    public IObservable<GetTitleDataResult> GetTitleData(GetTitleDataRequest request)
    {
        var observable = new Subject<GetTitleDataResult>();

        this.Publish(new GetTitleDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetTitleData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetTitleDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetTitleDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<GetTitleNewsResult> GetTitleNews(int? count)
    {
        var request = new GetTitleNewsRequest()
        {
            Count = count
        };
        return GetTitleNews(request);
    }

    public IObservable<GetTitleNewsResult> GetTitleNews(GetTitleNewsRequest request)
    {
        var observable = new Subject<GetTitleNewsResult>();

        this.Publish(new GetTitleNewsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetTitleNews(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetTitleNewsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetTitleNewsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<ModifyUserVirtualCurrencyResult> AddUserVirtualCurrency(int amount, string virtualCurrency)
    {
        var request = new AddUserVirtualCurrencyRequest()
        {
            Amount = amount,
            VirtualCurrency = virtualCurrency
        };
        return AddUserVirtualCurrency(request);
    }

    public IObservable<ModifyUserVirtualCurrencyResult> AddUserVirtualCurrency(AddUserVirtualCurrencyRequest request)
    {
        var observable = new Subject<ModifyUserVirtualCurrencyResult>();

        this.Publish(new AddUserVirtualCurrencyEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.AddUserVirtualCurrency(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new AddUserVirtualCurrencyEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new AddUserVirtualCurrencyEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<ConsumeItemResult> ConsumeItem(int consumeCount, string itemInstanceId)
    {
        var request = new ConsumeItemRequest()
        {
            ConsumeCount = consumeCount,
            ItemInstanceId = itemInstanceId
        };
        return ConsumeItem(request);
    }

    public IObservable<ConsumeItemResult> ConsumeItem(ConsumeItemRequest request)
    {
        var observable = new Subject<ConsumeItemResult>();

        this.Publish(new ConsumeItemEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.ConsumeItem(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new ConsumeItemEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new ConsumeItemEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<GetUserInventoryResult> GetUserInventory(int consumeCount, string itemInstanceId)
    {
        var request = new GetUserInventoryRequest();
        return GetUserInventory(request);
    }

    public IObservable<GetUserInventoryResult> GetUserInventory(GetUserInventoryRequest request)
    {
        var observable = new Subject<GetUserInventoryResult>();

        this.Publish(new GetUserInventoryEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetUserInventory(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetUserInventoryEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetUserInventoryEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<RedeemCouponResult> RedeemCoupon(string catalogVersion, string couponCode)
    {
        var request = new RedeemCouponRequest()
        {
            CatalogVersion = catalogVersion,
            CouponCode = couponCode
        };
        return RedeemCoupon(request);
    }

    public IObservable<RedeemCouponResult> RedeemCoupon(RedeemCouponRequest request)
    {
        var observable = new Subject<RedeemCouponResult>();

        this.Publish(new RedeemCouponEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RedeemCoupon(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RedeemCouponEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RedeemCouponEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<ReportPlayerClientResult> ReportPlayer(string reporteeId, string comment = null)
    {
        var request = new ReportPlayerClientRequest()
        {
            Comment = comment,
            ReporteeId = reporteeId
        };
        return ReportPlayer(request);
    }

    public IObservable<ReportPlayerClientResult> ReportPlayer(ReportPlayerClientRequest request)
    {
        var observable = new Subject<ReportPlayerClientResult>();

        this.Publish(new ReportPlayerEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.ReportPlayer(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new ReportPlayerEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new ReportPlayerEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<ModifyUserVirtualCurrencyResult> SubtractUserVirtualCurrency(int amount, string virtualCurrency)
    {
        var request = new SubtractUserVirtualCurrencyRequest()
        {
            Amount = amount,
            VirtualCurrency = virtualCurrency
        };
        return SubtractUserVirtualCurrency(request);
    }

    public IObservable<ModifyUserVirtualCurrencyResult> SubtractUserVirtualCurrency(SubtractUserVirtualCurrencyRequest request)
    {
        var observable = new Subject<ModifyUserVirtualCurrencyResult>();

        this.Publish(new SubtractUserVirtualCurrencyEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.SubtractUserVirtualCurrency(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new SubtractUserVirtualCurrencyEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new SubtractUserVirtualCurrencyEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<UnlockContainerItemResult> UnlockContainerItem(string catalogVersion, string containerItemId)
    {
        var request = new UnlockContainerItemRequest()
        {
            CatalogVersion = catalogVersion,
            ContainerItemId = containerItemId
        };
        return UnlockContainerItem(request);
    }

    public IObservable<UnlockContainerItemResult> UnlockContainerItem(UnlockContainerItemRequest request)
    {
        var observable = new Subject<UnlockContainerItemResult>();

        this.Publish(new UnlockContainerItemEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UnlockContainerItem(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UnlockContainerItemEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UnlockContainerItemEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<StartPurchaseResult> StartPurchase(List<ItemPuchaseRequest> items, string catalogVersion = null, string storeId = null)
    {
        var request = new StartPurchaseRequest()
        {
            Items = items,
            CatalogVersion = catalogVersion,
            StoreId = storeId
        };
        return StartPurchase(request);
    }

    public IObservable<StartPurchaseResult> StartPurchase(StartPurchaseRequest request)
    {
        var observable = new Subject<StartPurchaseResult>();

        this.Publish(new StartPurchaseEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.StartPurchase(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new StartPurchaseEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new StartPurchaseEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<PayForPurchaseResult> PayForPurchase(string orderId, string currency, string providerName)
    {
        var request = new PayForPurchaseRequest()
        {
            OrderId = orderId,
            Currency = currency,
            ProviderName = providerName
        };
        return PayForPurchase(request);
    }

    public IObservable<PayForPurchaseResult> PayForPurchase(PayForPurchaseRequest request)
    {
        var observable = new Subject<PayForPurchaseResult>();

        this.Publish(new PayForPurchaseEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.PayForPurchase(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new PayForPurchaseEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new PayForPurchaseEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<ConfirmPurchaseResult> ConfirmPurchase(string orderId)
    {
        var request = new ConfirmPurchaseRequest()
        {
            OrderId = orderId
        };
        return ConfirmPurchase(request);
    }

    public IObservable<ConfirmPurchaseResult> ConfirmPurchase(ConfirmPurchaseRequest request)
    {
        var observable = new Subject<ConfirmPurchaseResult>();

        this.Publish(new ConfirmPurchaseEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.ConfirmPurchase(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new ConfirmPurchaseEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new ConfirmPurchaseEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<PurchaseItemResult> PurchaseItem(string itemId, int price, string virtualCurrency, string catalogVersion, string storeId)
    {
        var request = new PurchaseItemRequest()
        {
            ItemId = itemId,
            Price = price,
            VirtualCurrency = virtualCurrency,
            CatalogVersion = catalogVersion,
            StoreId = storeId
        };
        return PurchaseItem(request);
    }

    public IObservable<PurchaseItemResult> PurchaseItem(PurchaseItemRequest request)
    {
        var observable = new Subject<PurchaseItemResult>();

        this.Publish(new PurchaseItemEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.PurchaseItem(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new PurchaseItemEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new PurchaseItemEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<AddFriendResult> AddFriend(string playFabId, string email = null, string titleDisplayName = null, string username = null)
    {
        var request = new AddFriendRequest()
        {
            FriendEmail = email,
            FriendPlayFabId = playFabId,
            FriendTitleDisplayName = titleDisplayName,
            FriendUsername = username
        };
        return AddFriend(request);
    }

    public IObservable<AddFriendResult> AddFriend(AddFriendRequest request)
    {
        var observable = new Subject<AddFriendResult>();

        this.Publish(new AddFriendEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.AddFriend(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new AddFriendEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new AddFriendEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetFriendsListResult> GetFriendsList(bool? includeSteamFriends = null)
    {
        var request = new GetFriendsListRequest()
        {
            IncludeSteamFriends = includeSteamFriends
        };
        return GetFriendsList(request);
    }

    public IObservable<GetFriendsListResult> GetFriendsList(GetFriendsListRequest request)
    {
        var observable = new Subject<GetFriendsListResult>();

        this.Publish(new GetFriendsListEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetFriendsList(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetFriendsListEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetFriendsListEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<RemoveFriendResult> RemoveFriend(string playFabId)
    {
        var request = new RemoveFriendRequest()
        {
            FriendPlayFabId = playFabId
        };
        return RemoveFriend(request);
    }

    public IObservable<RemoveFriendResult> RemoveFriend(RemoveFriendRequest request)
    {
        var observable = new Subject<RemoveFriendResult>();

        this.Publish(new RemoveFriendEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RemoveFriend(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RemoveFriendEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RemoveFriendEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<SetFriendTagsResult> SetFriendTags(string playFabId, List<string> tags)
    {
        var request = new SetFriendTagsRequest()
        {
            FriendPlayFabId = playFabId,
            Tags = tags
        };
        return SetFriendTags(request);
    }

    public IObservable<SetFriendTagsResult> SetFriendTags(SetFriendTagsRequest request)
    {
        var observable = new Subject<SetFriendTagsResult>();

        this.Publish(new SetFriendTagsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.SetFriendTags(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new SetFriendTagsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new SetFriendTagsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<RegisterForIOSPushNotificationResult> RegisterForIOSPushNotification(string deviceToken, string confirmationMessage, bool? sendPushNotificationConfirmation)
    {
        var request = new RegisterForIOSPushNotificationRequest()
        {
            ConfirmationMessage = confirmationMessage,
            DeviceToken = deviceToken,
            SendPushNotificationConfirmation = sendPushNotificationConfirmation
        };
        return RegisterForIOSPushNotification(request);
    }

    public IObservable<RegisterForIOSPushNotificationResult> RegisterForIOSPushNotification(RegisterForIOSPushNotificationRequest request)
    {
        var observable = new Subject<RegisterForIOSPushNotificationResult>();

        this.Publish(new RegisterForIOSPushNotificationEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RegisterForIOSPushNotification(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RegisterForIOSPushNotificationEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RegisterForIOSPushNotificationEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<RestoreIOSPurchasesResult> RestoreIOSPurchases(string receiptData)
    {
        var request = new RestoreIOSPurchasesRequest()
        {
            ReceiptData = receiptData
        };
        return RestoreIOSPurchases(request);
    }

    public IObservable<RestoreIOSPurchasesResult> RestoreIOSPurchases(RestoreIOSPurchasesRequest request)
    {
        var observable = new Subject<RestoreIOSPurchasesResult>();

        this.Publish(new RestoreIOSPurchasesEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RestoreIOSPurchases(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RestoreIOSPurchasesEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RestoreIOSPurchasesEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<ValidateIOSReceiptResult> ValidateIOSReceipt(string receiptData, int purchasePrice, string currencyCode)
    {
        var request = new ValidateIOSReceiptRequest()
        {
            CurrencyCode = currencyCode,
            ReceiptData = receiptData,
            PurchasePrice = purchasePrice
        };
        return ValidateIOSReceipt(request);
    }

    public IObservable<ValidateIOSReceiptResult> ValidateIOSReceipt(ValidateIOSReceiptRequest request)
    {
        var observable = new Subject<ValidateIOSReceiptResult>();

        this.Publish(new ValidateIOSReceiptEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.ValidateIOSReceipt(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new ValidateIOSReceiptEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new ValidateIOSReceiptEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<CurrentGamesResult> GetCurrentGames(string buildVersion, Region? region)
    {
        var request = new CurrentGamesRequest()
        {
            BuildVersion = buildVersion,
            Region = region
        };
        return GetCurrentGames(request);
    }

    public IObservable<CurrentGamesResult> GetCurrentGames(CurrentGamesRequest request)
    {
        var observable = new Subject<CurrentGamesResult>();

        this.Publish(new GetCurrentGamesEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetCurrentGames(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetCurrentGamesEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetCurrentGamesEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GameServerRegionsResult> GetGameServerRegions(string buildVersion, string titleId)
    {
        var request = new GameServerRegionsRequest()
        {
            TitleId = TitleId,
            BuildVersion = buildVersion
        };
        return GetGameServerRegions(request);
    }

    public IObservable<GameServerRegionsResult> GetGameServerRegions(GameServerRegionsRequest request)
    {
        var observable = new Subject<GameServerRegionsResult>();

        this.Publish(new GetGameServerRegionsEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetGameServerRegions(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetGameServerRegionsEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetGameServerRegionsEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<MatchmakeResult> Matchmake(string lobbyId = null, string gameMode = null, bool? enableQueue = null, string buildVersion = null, Region? region = null)
    {
        var request = new MatchmakeRequest()
        {
            BuildVersion = buildVersion,
            EnableQueue = enableQueue,
            GameMode = gameMode,
            LobbyId = lobbyId,
            Region = region
        };
        return Matchmake(request);
    }

    public IObservable<MatchmakeResult> Matchmake(MatchmakeRequest request)
    {
        var observable = new Subject<MatchmakeResult>();

        this.Publish(new MatchmakeEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.Matchmake(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new MatchmakeEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new MatchmakeEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<StartGameResult> StartGame(string gameMode, Region region, string buildVersion, string customCommandLineData)
    {
        var request = new StartGameRequest()
        {
            BuildVersion = buildVersion,
            Region = region,
            GameMode = gameMode,
            CustomCommandLineData = customCommandLineData
        };
        return StartGame(request);
    }

    public IObservable<StartGameResult> StartGame(StartGameRequest request)
    {
        var observable = new Subject<StartGameResult>();

        this.Publish(new StartGameEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.StartGame(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new StartGameEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new StartGameEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<AndroidDevicePushNotificationRegistrationResult> AndroidDevicePushNotificationRegistration(string deviceToken, string confirmationMessage, bool? sendPushNotificationConfirmation)
    {
        var request = new AndroidDevicePushNotificationRegistrationRequest()
        {
            ConfirmationMessege = confirmationMessage,
            DeviceToken = deviceToken,
            SendPushNotificationConfirmation = sendPushNotificationConfirmation
        };
        return AndroidDevicePushNotificationRegistration(request);
    }

    public IObservable<AndroidDevicePushNotificationRegistrationResult> AndroidDevicePushNotificationRegistration(AndroidDevicePushNotificationRegistrationRequest request)
    {
        var observable = new Subject<AndroidDevicePushNotificationRegistrationResult>();

        this.Publish(new AndroidDevicePushNotificationRegistrationEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.AndroidDevicePushNotificationRegistration(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new AndroidDevicePushNotificationRegistrationEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new AndroidDevicePushNotificationRegistrationEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<ValidateGooglePlayPurchaseResult> ValidateGooglePlayPurchase(string receiptJson, string signature)
    {
        var request = new ValidateGooglePlayPurchaseRequest()
        {
            ReceiptJson = receiptJson,
            Signature = signature
        };
        return ValidateGooglePlayPurchase(request);
    }

    public IObservable<ValidateGooglePlayPurchaseResult> ValidateGooglePlayPurchase(ValidateGooglePlayPurchaseRequest request)
    {
        var observable = new Subject<ValidateGooglePlayPurchaseResult>();

        this.Publish(new ValidateGooglePlayPurchaseEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.ValidateGooglePlayPurchase(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new ValidateGooglePlayPurchaseEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new ValidateGooglePlayPurchaseEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<LogEventResult> LogEvent(Dictionary<string, object> body, string eventName)
    {
        var request = new LogEventRequest()
        {
            Body = body,
            eventName = eventName
        };
        return LogEvent(request);
    }

    public IObservable<LogEventResult> LogEvent(LogEventRequest request)
    {
        var observable = new Subject<LogEventResult>();

        this.Publish(new LogEventEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.LogEvent(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new LogEventEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new LogEventEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<AddSharedGroupMembersResult> AddSharedGroupMembers(string sharedGroupId, List<string> playFabIds = null)
    {
        var request = new AddSharedGroupMembersRequest()
        {
            SharedGroupId = sharedGroupId,
            PlayFabIds = playFabIds
        };
        return AddSharedGroupMembers(request);
    }

    public IObservable<AddSharedGroupMembersResult> AddSharedGroupMembers(AddSharedGroupMembersRequest request)
    {
        var observable = new Subject<AddSharedGroupMembersResult>();

        this.Publish(new AddSharedGroupMembersEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.AddSharedGroupMembers(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new AddSharedGroupMembersEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new AddSharedGroupMembersEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }
    public IObservable<CreateSharedGroupResult> CreateSharedGroup(string sharedGroupId)
    {
        var request = new CreateSharedGroupRequest()
        {
            SharedGroupId = sharedGroupId
        };
        return CreateSharedGroup(request);
    }

    public IObservable<CreateSharedGroupResult> CreateSharedGroup(CreateSharedGroupRequest request)
    {
        var observable = new Subject<CreateSharedGroupResult>();

        this.Publish(new CreateSharedGroupEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.CreateSharedGroup(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new CreateSharedGroupEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new CreateSharedGroupEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetPublisherDataResult> GetPublisherData(List<string> keys)
    {
        var request = new GetPublisherDataRequest()
        {
            Keys = keys
        };
        return GetPublisherData(request);
    }

    public IObservable<GetPublisherDataResult> GetPublisherData(GetPublisherDataRequest request)
    {
        var observable = new Subject<GetPublisherDataResult>();

        this.Publish(new GetPublisherDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetPublisherData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetPublisherDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetPublisherDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<RemoveSharedGroupMembersResult> RemoveSharedGroupMembers(string sharedGroupId, List<string> playFabIds)
    {
        var request = new RemoveSharedGroupMembersRequest()
        {
            PlayFabIds = playFabIds,
            SharedGroupId = sharedGroupId
        };
        return RemoveSharedGroupMembers(request);
    }

    public IObservable<RemoveSharedGroupMembersResult> RemoveSharedGroupMembers(RemoveSharedGroupMembersRequest request)
    {
        var observable = new Subject<RemoveSharedGroupMembersResult>();

        this.Publish(new RemoveSharedGroupMembersEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RemoveSharedGroupMembers(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RemoveSharedGroupMembersEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RemoveSharedGroupMembersEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<UpdateSharedGroupDataResult> UpdateSharedGroupData(Dictionary<string, string> data, UserDataPermission? permission)
    {
        var request = new UpdateSharedGroupDataRequest()
        {
            Data = data,
            Permission = permission
        };
        return UpdateSharedGroupData(request);
    }

    public IObservable<UpdateSharedGroupDataResult> UpdateSharedGroupData(UpdateSharedGroupDataRequest request)
    {
        var observable = new Subject<UpdateSharedGroupDataResult>();

        this.Publish(new UpdateSharedGroupDataEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.UpdateSharedGroupData(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new UpdateSharedGroupDataEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new UpdateSharedGroupDataEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


    public IObservable<EmptyResult> RefreshPSNAuthToken(Dictionary<string, string> data, UserDataPermission? permission)
    {
        var request = new RefreshPSNAuthTokenRequest()
        {
        };
        return RefreshPSNAuthToken(request);
    }

    public IObservable<EmptyResult> RefreshPSNAuthToken(RefreshPSNAuthTokenRequest request)
    {
        var observable = new Subject<EmptyResult>();

        this.Publish(new RefreshPSNAuthTokenEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RefreshPSNAuthToken(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RefreshPSNAuthTokenEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RefreshPSNAuthTokenEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetCloudScriptUrlResult> GetCloudScriptUrl(bool? testing, int? version)
    {
        var request = new GetCloudScriptUrlRequest()
        {
            Testing = testing,
            Version = version

        };
        return GetCloudScriptUrl(request);
    }

    public IObservable<GetCloudScriptUrlResult> GetCloudScriptUrl(GetCloudScriptUrlRequest request)
    {
        var observable = new Subject<GetCloudScriptUrlResult>();

        this.Publish(new GetCloudScriptUrlEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetCloudScriptUrl(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetCloudScriptUrlEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetCloudScriptUrlEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<RunCloudScriptResult> RunCloudScript(Dictionary<string, string> data, UserDataPermission? permission)
    {
        var request = new RunCloudScriptRequest()
        {
        };
        return RunCloudScript(request);
    }

    public IObservable<RunCloudScriptResult> RunCloudScript(RunCloudScriptRequest request)
    {
        var observable = new Subject<RunCloudScriptResult>();

        this.Publish(new RunCloudScriptEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.RunCloudScript(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new RunCloudScriptEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new RunCloudScriptEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }

    public IObservable<GetContentDownloadUrlResult> GetContentDownloadUrl()
    {
        var request = new GetContentDownloadUrlRequest()
        {
        };
        return GetContentDownloadUrl(request);
    }

    public IObservable<GetContentDownloadUrlResult> GetContentDownloadUrl(GetContentDownloadUrlRequest request)
    {
        var observable = new Subject<GetContentDownloadUrlResult>();

        this.Publish(new GetContentDownloadUrlEvent()
        {
            OperationState = PlayFabOperationState.Started,
            Request = request
        });

        PlayFabClientAPI.GetContentDownloadUrl(request,
            result =>
            {
                observable.OnNext(result);
                this.Publish(new GetContentDownloadUrlEvent()
                {
                    Result = result,
                    Request = request,
                    OperationState = PlayFabOperationState.Complete
                });
            },
            error =>
            {
                observable.OnError(new PlayfabException(error));
                this.Publish(new GetContentDownloadUrlEvent()
                {
                    Request = request,
                    Error = error,
                    OperationState = PlayFabOperationState.Failed
                });
            });

        return observable.Take(1);
    }


}

public class PlayfabException : Exception
{
    public PlayfabException(string message, PlayFabError error)
        : base(message)
    {
        Error = error;
    }

    public PlayFabError Error { get; set; }

    public PlayfabException(PlayFabError error)
    {
        Error = error;
    }
}

public static class PlayFabErrorExtensions
{
    public static string Print(this PlayFabError error)
    {
        var result = string.Format("Code: {0}\nMessage: {1}\nHttp Code: {2}\nHttp Status: {3}\n", error.Error,
            error.ErrorMessage, error.HttpCode, error.HttpStatus);

        if(error.ErrorDetails!= null)
        foreach (var key in error.ErrorDetails.Keys)
        {
            result += string.Format(" . {0}:\n", key);
            foreach (var message in error.ErrorDetails[key])
            {
                result += string.Format("    {0}\n", message);                
            }
        }

        return result;
    }
}


namespace PlayFabServiceSystem
{
    public class PlayFabServiceOperationEventBase<TRequest, TResult> : PlayFabServiceOperationEventBase
    {

        public TRequest Request { get; set; }
        public TResult Result { get; set; }
    }

    public class PlayFabServiceOperationEventBase
    {
        public PlayFabOperationState OperationState { get; set; }

        public PlayFabError Error { get; set; }

    }

    public class LoginEvent : PlayFabServiceOperationEventBase<object, LoginResult>
    {
        public PlayFabAuthType AuthType { get; set; }
    }

    public class RegisterPlayFabUserEvent :
        PlayFabServiceOperationEventBase<RegisterPlayFabUserRequest, RegisterPlayFabUserResult>
    {
    }

    public class AddUsernamePasswordEvent :
        PlayFabServiceOperationEventBase<AddUsernamePasswordRequest, AddUsernamePasswordResult>
    {
    }

    public class GetAccountInfoEvent : PlayFabServiceOperationEventBase<GetAccountInfoRequest, GetAccountInfoResult>
    {
    }

    public class GetPlayFabIDsFromFacebookIDsEvent :
        PlayFabServiceOperationEventBase<GetPlayFabIDsFromFacebookIDsRequest, GetPlayFabIDsFromFacebookIDsResult>
    {
    }

    public class GetUserCombinedInfoEvent :
        PlayFabServiceOperationEventBase<GetUserCombinedInfoRequest, GetUserCombinedInfoResult>
    {
    }

    public class LinkAndroidDeviceIDEvent :
        PlayFabServiceOperationEventBase<LinkAndroidDeviceIDRequest, LinkAndroidDeviceIDResult>
    {
    }

    public class LinkFacebookAccountEvent :
        PlayFabServiceOperationEventBase<LinkFacebookAccountRequest, LinkFacebookAccountResult>
    {
    }

    public class LinkGameCenterAccountEvent :
        PlayFabServiceOperationEventBase<LinkGameCenterAccountRequest, LinkGameCenterAccountResult>
    {
    }

    public class LinkIOSDeviceIDEvent : PlayFabServiceOperationEventBase<LinkIOSDeviceIDRequest, LinkIOSDeviceIDResult>
    {
    }

    public class LinkSteamAccountEvent :
        PlayFabServiceOperationEventBase<LinkSteamAccountRequest, LinkSteamAccountResult>
    {
    }

    public class SendAccountRecoveryEmailEvent :
        PlayFabServiceOperationEventBase<SendAccountRecoveryEmailRequest, SendAccountRecoveryEmailResult>
    {
    }

    public class UnlinkAndroidDeviceIDEvent :
        PlayFabServiceOperationEventBase<UnlinkAndroidDeviceIDRequest, UnlinkAndroidDeviceIDResult>
    {
    }

    public class UnlinkFacebookAccountEvent :
        PlayFabServiceOperationEventBase<UnlinkFacebookAccountRequest, UnlinkFacebookAccountResult>
    {
    }

    public class UnlinkGameCenterAccountEvent :
        PlayFabServiceOperationEventBase<UnlinkGameCenterAccountRequest, UnlinkGameCenterAccountResult>
    {
    }

    public class UnlinkIOSDeviceIDEvent :
        PlayFabServiceOperationEventBase<UnlinkIOSDeviceIDRequest, UnlinkIOSDeviceIDResult>
    {
    }

    public class UnlinkSteamAccountEvent :
        PlayFabServiceOperationEventBase<UnlinkSteamAccountRequest, UnlinkSteamAccountResult>
    {
    }

    public class UpdateUserTitleDisplayNameEvent :
        PlayFabServiceOperationEventBase<UpdateUserTitleDisplayNameRequest, UpdateUserTitleDisplayNameResult>
    {
    }

    public class GetFriendLeaderboardEvent :
        PlayFabServiceOperationEventBase<GetFriendLeaderboardRequest, GetLeaderboardResult>
    {
    }

    public class GetLeaderboardEvent : PlayFabServiceOperationEventBase<GetLeaderboardRequest, GetLeaderboardResult>
    {
    }

    public class GetLeaderboardAroundCurrentUserEvent :
        PlayFabServiceOperationEventBase<GetLeaderboardAroundCurrentUserRequest, GetLeaderboardAroundCurrentUserResult>
    {
    }

    public class GetUserDataEvent : PlayFabServiceOperationEventBase<GetUserDataRequest, GetUserDataResult>
    {
    }

    public class GetUserPublisherDataEvent : PlayFabServiceOperationEventBase<GetUserDataRequest, GetUserDataResult>
    {
    }

    public class GetUserPublisherReadOnlyDataEvent :
        PlayFabServiceOperationEventBase<GetUserDataRequest, GetUserDataResult>
    {
    }

    public class GetUserReadOnlyDataEvent : PlayFabServiceOperationEventBase<GetUserDataRequest, GetUserDataResult>
    {
    }

    public class GetUserStatisticsEvent :
        PlayFabServiceOperationEventBase<GetUserStatisticsRequest, GetUserStatisticsResult>
    {
    }

    public class UpdateUserDataEvent : PlayFabServiceOperationEventBase<UpdateUserDataRequest, UpdateUserDataResult>
    {
    }

    public class UpdateUserPublisherDataEvent :
        PlayFabServiceOperationEventBase<UpdateUserDataRequest, UpdateUserDataResult>
    {
    }

    public class UpdateUserStatisticsEvent :
        PlayFabServiceOperationEventBase<UpdateUserStatisticsRequest, UpdateUserStatisticsResult>
    {
    }

    public class GetCatalogItemsEvent : PlayFabServiceOperationEventBase<GetCatalogItemsRequest, GetCatalogItemsResult>
    {
    }

    public class GetStoreItemsEvent : PlayFabServiceOperationEventBase<GetStoreItemsRequest, GetStoreItemsResult>
    {
    }

    public class GetTitleDataEvent : PlayFabServiceOperationEventBase<GetTitleDataRequest, GetTitleDataResult>
    {
    }

    public class GetTitleNewsEvent : PlayFabServiceOperationEventBase<GetTitleNewsRequest, GetTitleNewsResult>
    {
    }

    public class AddUserVirtualCurrencyEvent :
        PlayFabServiceOperationEventBase<AddUserVirtualCurrencyRequest, ModifyUserVirtualCurrencyResult>
    {
    }

    public class ConsumeItemEvent : PlayFabServiceOperationEventBase<ConsumeItemRequest, ConsumeItemResult>
    {
    }

    public class GetUserInventoryEvent :
        PlayFabServiceOperationEventBase<GetUserInventoryRequest, GetUserInventoryResult>
    {
    }

    public class RedeemCouponEvent : PlayFabServiceOperationEventBase<RedeemCouponRequest, RedeemCouponResult>
    {
    }

    public class ReportPlayerEvent :
        PlayFabServiceOperationEventBase<ReportPlayerClientRequest, ReportPlayerClientResult>
    {
    }

    public class SubtractUserVirtualCurrencyEvent :
        PlayFabServiceOperationEventBase<SubtractUserVirtualCurrencyRequest, ModifyUserVirtualCurrencyResult>
    {
    }

    public class UnlockContainerItemEvent :
        PlayFabServiceOperationEventBase<UnlockContainerItemRequest, UnlockContainerItemResult>
    {
    }

    public class StartPurchaseEvent : PlayFabServiceOperationEventBase<StartPurchaseRequest, StartPurchaseResult>
    {
    }

    public class PayForPurchaseEvent : PlayFabServiceOperationEventBase<PayForPurchaseRequest, PayForPurchaseResult>
    {
    }

    public class ConfirmPurchaseEvent : PlayFabServiceOperationEventBase<ConfirmPurchaseRequest, ConfirmPurchaseResult>
    {
    }

    public class PurchaseItemEvent : PlayFabServiceOperationEventBase<PurchaseItemRequest, PurchaseItemResult>
    {
    }

    public class AddFriendEvent : PlayFabServiceOperationEventBase<AddFriendRequest, AddFriendResult>
    {
    }

    public class GetFriendsListEvent : PlayFabServiceOperationEventBase<GetFriendsListRequest, GetFriendsListResult>
    {
    }

    public class RemoveFriendEvent : PlayFabServiceOperationEventBase<RemoveFriendRequest, RemoveFriendResult>
    {
    }

    public class SetFriendTagsEvent : PlayFabServiceOperationEventBase<SetFriendTagsRequest, SetFriendTagsResult>
    {
    }

    public class RegisterForIOSPushNotificationEvent :
        PlayFabServiceOperationEventBase<RegisterForIOSPushNotificationRequest, RegisterForIOSPushNotificationResult>
    {
    }

    public class RestoreIOSPurchasesEvent :
        PlayFabServiceOperationEventBase<RestoreIOSPurchasesRequest, RestoreIOSPurchasesResult>
    {
    }

    public class ValidateIOSReceiptEvent :
        PlayFabServiceOperationEventBase<ValidateIOSReceiptRequest, ValidateIOSReceiptResult>
    {
    }

    public class GetCurrentGamesEvent : PlayFabServiceOperationEventBase<CurrentGamesRequest, CurrentGamesResult>
    {
    }

    public class GetGameServerRegionsEvent :
        PlayFabServiceOperationEventBase<GameServerRegionsRequest, GameServerRegionsResult>
    {
    }

    public class MatchmakeEvent : PlayFabServiceOperationEventBase<MatchmakeRequest, MatchmakeResult>
    {
    }

    public class StartGameEvent : PlayFabServiceOperationEventBase<StartGameRequest, StartGameResult>
    {
    }

    public class AndroidDevicePushNotificationRegistrationEvent :
        PlayFabServiceOperationEventBase
            <AndroidDevicePushNotificationRegistrationRequest, AndroidDevicePushNotificationRegistrationResult>
    {
    }

    public class ValidateGooglePlayPurchaseEvent :
        PlayFabServiceOperationEventBase<ValidateGooglePlayPurchaseRequest, ValidateGooglePlayPurchaseResult>
    {
    }

    public class LogEventEvent : PlayFabServiceOperationEventBase<LogEventRequest, LogEventResult>
    {
    }

    public class AddSharedGroupMembersEvent :
        PlayFabServiceOperationEventBase<AddSharedGroupMembersRequest, AddSharedGroupMembersResult>
    {
    }

    public class CreateSharedGroupEvent :
        PlayFabServiceOperationEventBase<CreateSharedGroupRequest, CreateSharedGroupResult>
    {
    }

    public class GetPublisherDataEvent :
        PlayFabServiceOperationEventBase<GetPublisherDataRequest, GetPublisherDataResult>
    {
    }

    public class GetSharedGroupDataEvent :
        PlayFabServiceOperationEventBase<GetSharedGroupDataRequest, GetSharedGroupDataResult>
    {
    }

    public class RemoveSharedGroupMembersEvent :
        PlayFabServiceOperationEventBase<RemoveSharedGroupMembersRequest, RemoveSharedGroupMembersResult>
    {
    }

    public class UpdateSharedGroupDataEvent :
        PlayFabServiceOperationEventBase<UpdateSharedGroupDataRequest, UpdateSharedGroupDataResult>
    {
    }

    public class RefreshPSNAuthTokenEvent : PlayFabServiceOperationEventBase<RefreshPSNAuthTokenRequest, EmptyResult>
    {
    }

    public class GetCloudScriptUrlEvent :
        PlayFabServiceOperationEventBase<GetCloudScriptUrlRequest, GetCloudScriptUrlResult>
    {
    }

    public class RunCloudScriptEvent : PlayFabServiceOperationEventBase<RunCloudScriptRequest, RunCloudScriptResult>
    {
    }

    public class GetContentDownloadUrlEvent :
        PlayFabServiceOperationEventBase<GetContentDownloadUrlRequest, GetContentDownloadUrlResult>
    {
    }
}
