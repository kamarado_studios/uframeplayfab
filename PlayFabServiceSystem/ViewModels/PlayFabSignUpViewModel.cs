using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using UnityEngine;
using UniRx;
public partial class PlayFabSignUpViewModel : PlayFabSignUpViewModelBase
{
	private System.IDisposable _CanTrySignUpDisposable;
	public override void Bind()
	{
		base.Bind();
	}
	public override Boolean ComputeCanTrySignUp()
	{
		return IsGoodPassword(Password) && IsGoodUsername(Username) && IsEmail(Email);
	}
	private bool IsEmail(string email)
	{
		if (email == null)
			return false;
		try {
			MailAddress m = new MailAddress(email);
			return true;
		} catch (FormatException) {
			return false;
		}
	}
	private bool IsGoodUsername(string username)
	{
		return username != null && username.Length >= 6;
	}
	private bool IsGoodPassword(string password)
	{
		return password != null && password.Length >= 6;
	}
}
