using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFabServiceSystem;
using UniRx;
using UnityEngine;
public class PlayFabSignUpView : PlayFabSignUpViewBase
{
	protected override void InitializeViewModel(ViewModel model)
	{
		base.InitializeViewModel(model);
	}
	public override void Bind()
	{
		base.Bind();
	}
	public override void CanTrySignUpChanged(bool arg1)
	{
		base.CanTrySignUpChanged(arg1);
		this._TrySignUpButton.gameObject.SetActive(arg1);
	}
}
