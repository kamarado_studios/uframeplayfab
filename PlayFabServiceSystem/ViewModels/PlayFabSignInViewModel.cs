using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using UnityEngine;
using UniRx;
public partial class PlayFabSignInViewModel : PlayFabSignInViewModelBase
{
	public override void Bind()
	{
		base.Bind();
	}
	public override bool ComputeCanTrySignIn()
	{
		return IsGoodPassword(Password) && IsGoodUsername(Username);
	}
	private bool IsGoodUsername(string username)
	{
		return username != null && username.Length >= 6;
	}
	private bool IsGoodPassword(string password)
	{
		return password != null && password.Length >= 6;
	}
}
