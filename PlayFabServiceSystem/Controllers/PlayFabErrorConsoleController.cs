using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFabServiceSystem;
using UniRx;
public class PlayFabErrorConsoleController : PlayFabErrorConsoleControllerBase
{
	public override void Setup()
	{
		base.Setup();
	}
	public override void InitializePlayFabErrorConsole(PlayFabErrorConsoleViewModel viewModel)
	{
		base.InitializePlayFabErrorConsole(viewModel);
	}
//	public override void PlayFabServiceOperationEventBaseHandler(PlayFabServiceOperationEventBase command)
//	{
//		base.PlayFabServiceOperationEventBaseHandler(command);
//		PlayFabErrorConsoleViewModels.ToList().ForEach(c =>
//		{
//			c.Messages.Add(string.Format("{0} was {1}!", command.GetType().Name.Replace("Event", ""), command.OperationState));
//			if (command.Error != null) {
//				c.Messages.Add(command.Error.ErrorMessage);
//				if (command.Error.ErrorDetails != null)
//					foreach (var errorDetail in command.Error.ErrorDetails) {
//						c.Messages.Add(string.Format("{0} : {1}", errorDetail.Key, errorDetail.Value));
//					}
//			}
//		});
//	}
}
