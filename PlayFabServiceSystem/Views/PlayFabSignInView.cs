using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFabServiceSystem;
using UniRx;
using UnityEngine;
public class PlayFabSignInView : PlayFabSignInViewBase
{
	protected override void InitializeViewModel(ViewModel model)
	{
		base.InitializeViewModel(model);
	}
	public override void Bind()
	{
		base.Bind();
	}
	public override void CanTrySignInChanged(bool arg1)
	{
		this._TrySignInButton.gameObject.SetActive(arg1);
	}
}
