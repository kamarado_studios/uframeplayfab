using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
public class PlayFabSignInController : PlayFabSignInControllerBase
{
	[Inject()]
	public PlayFabService PlayFabService { get; set; }
	public override void Setup()
	{
		base.Setup();
	}
	public override void InitializePlayFabSignIn(PlayFabSignInViewModel viewModel)
	{
		base.InitializePlayFabSignIn(viewModel);
	}
	public override void TrySignIn(PlayFabSignInViewModel viewModel)
	{
		base.TrySignIn(viewModel);
		PlayFabService.LoginWithPlayFab(new LoginWithPlayFabRequest {
			Password = viewModel.Password,
			Username = viewModel.Username
		}).Subscribe(result => { Debug.Log(string.Format("{0} ({1}) has been logged! SessionTicket: {2}", viewModel.Username, result.PlayFabId, result.SessionTicket)); }, er =>
		{
			var error = (er as PlayfabException).Error;
			Debug.Log(string.Format("Error while logging in {0}: {1},{2}", viewModel.Username, error.Error, error.ErrorMessage));
		});
	}
	public override void TrySignInHandler(TrySignInCommand command)
	{
		base.TrySignInHandler(command);
	}
}
