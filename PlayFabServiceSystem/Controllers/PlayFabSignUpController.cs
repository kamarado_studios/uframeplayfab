using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
public class PlayFabSignUpController : PlayFabSignUpControllerBase
{
	[Inject()]
	public PlayFabService PlayFabService { get; set; }
	public override void Setup()
	{
		base.Setup();
	}
	public override void InitializePlayFabSignUp(PlayFabSignUpViewModel viewModel)
	{
		base.InitializePlayFabSignUp(viewModel);
	}
	public override void TrySignUp(PlayFabSignUpViewModel viewModel)
	{
		base.TrySignUp(viewModel);
		PlayFabService.RegisterPlayFabUser(new RegisterPlayFabUserRequest {
			Email = viewModel.Email,
			Password = viewModel.Password,
			Username = viewModel.Username
		}).Subscribe(result => { Debug.Log(string.Format("{0} ({1}) is registered! Session Ticket: {2}", result.Username, result.PlayFabId, result.SessionTicket)); }, ex =>
		{
			var error = (ex as PlayfabException).Error;
			Debug.Log(string.Format("Error Occured while registering a user: {0}, {1}", error.Error, error.ErrorMessage));
		});
	}
	public override void TrySignUpHandler(TrySignUpCommand command)
	{
		base.TrySignUpHandler(command);
	}
}
